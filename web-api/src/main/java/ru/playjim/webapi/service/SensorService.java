package ru.playjim.webapi.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import ru.playjim.webapi.model.Sensor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SensorService {

    private final JdbcTemplate jdbcTemplate;

    public int count() {
        return jdbcTemplate.queryForObject("SELECT count(*) FROM sensor.sensor", Integer.class);
    }

    public Page<Sensor> findAll(Pageable page) {
        Sort.Order order = !page.getSort().isEmpty() ? page.getSort().toList().get(0) : Sort.Order.by("ID");
        List<Sensor> sensors = jdbcTemplate.query("SELECT * FROM SENSOR.SENSOR ORDER BY " + order.getProperty() + " "
                        + order.getDirection().name() + " LIMIT " + page.getPageSize() + " OFFSET " + page.getOffset(),
                (rs, rowNum) -> mapUserResult(rs));
        return new PageImpl<>(sensors, page, count());
    }

    public Page<Sensor> getSensorsByType(Pageable pageable) {
        return findAll(pageable);
    }

    private Sensor mapUserResult(ResultSet rs) throws SQLException {
        return new Sensor(rs.getLong("ID"),
                rs.getFloat("VALUE"),
                rs.getDate("TIMESTAMP"),
                rs.getString("TYPE"));
    }

}
