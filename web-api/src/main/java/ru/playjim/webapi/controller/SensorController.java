package ru.playjim.webapi.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.playjim.webapi.model.Sensor;
import ru.playjim.webapi.service.SensorService;

@RestController
@RequiredArgsConstructor
public class SensorController {
    private final SensorService sensorService;

    @GetMapping("/sensor")
    public Page<Sensor> findAllByType(Pageable pageable) {
        return sensorService.getSensorsByType(pageable);
    }
}
