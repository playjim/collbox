package ru.playjim.sensor.model;

import java.util.Date;

public record SensorDto(Long id,
                        Float value,
                        Date date,
                        String type) {
}
