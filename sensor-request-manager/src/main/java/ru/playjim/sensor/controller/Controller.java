package ru.playjim.sensor.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.playjim.sensor.model.SensorDto;
import ru.playjim.sensor.service.Producer;

@RestController
@Slf4j
@RequiredArgsConstructor
public class Controller {
    private final Producer producer;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/sensor", produces = MediaType.APPLICATION_JSON_VALUE)
    public void addSensor(@RequestBody SensorDto sensor) {
        log.info("Got sensor request {}", sensor);
        producer.sendSensor(sensor);
    }
}
