CREATE TABLE sensor.sensor
(
    id        serial PRIMARY KEY not null,
    value     real not null,
    timestamp timestamp   not null,
    type      varchar(50) not null
);