package ru.playjim.processor.model;

import java.util.Date;

public record Sensor(Long id, Float value, Date date, String type) {
}
