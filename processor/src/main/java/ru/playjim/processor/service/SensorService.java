package ru.playjim.processor.service;

import com.google.common.util.concurrent.AtomicDouble;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import ru.playjim.processor.model.Sensor;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class SensorService {

    private final JdbcTemplate jdbcTemplate;
    private final MeterRegistry registry;
    private final AtomicDouble co2 = new AtomicDouble(0);
    private final AtomicDouble temperature = new AtomicDouble(0);
    private final AtomicDouble humidity = new AtomicDouble(0);

    private final static String INSERT_SENSOR = "INSERT INTO sensor.sensor(VALUE, timestamp,TYPE) VALUES(?, ?, ?);";

    @KafkaListener(topics = "topic1", groupId = "group2")
    public void consume(Sensor sensor) {
        addProduct(sensor);
        if ("CO2".equals(sensor.type())) {
            co2.set(sensor.value());
            registry.gauge("sensor.CO2", co2);
        }
        if ("TEMPERATURE".equals(sensor.type())) {
            temperature.set(sensor.value());
            registry.gauge("sensor.TEMPERATURE", temperature);
        }
        if ("HUMIDITY".equals(sensor.type())) {
            humidity.set(sensor.value());
            registry.gauge("sensor.HUMIDITY", humidity);
        }
        log.info("received data in Consumer One {}", sensor);
    }

    @Transactional
    public void addProduct(Sensor sensor) {
        jdbcTemplate.update(INSERT_SENSOR, sensor.value(), sensor.date(), sensor.type());
    }

}
